###
### Code generator
### 
import os
import sys
import argparse
import pathlib

###
### Definitions
###
from source import main


###
### Set arguments
###
parser = argparse.ArgumentParser(description='Code generator')

parser.add_argument("file",nargs='*')

parser.add_argument("--prefix",  help="Prefix for translated filename.",default="")
parser.add_argument("--postfix", help="Postfix for translated filename.",default="__genmod")
parser.add_argument("--dest", help="Destination path for output file.",default="./")



###
### Main
###
args = parser.parse_args()

for target in args.file:
	targetfile = pathlib.Path(target)
	targetname = os.path.basename(targetfile)
	targetbody, targetext = os.path.splitext(targetname)
	output = args.dest + "/" + args.prefix + targetbody + args.postfix + targetext 
	outputfile = pathlib.Path(output).resolve()
	main.execute(target,outputfile)
	continue


