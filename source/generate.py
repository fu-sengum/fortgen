###
###
###
import sys
import itertools
import re
import copy
import yaml

###
###
###
from .define_keyword import key
from .define_keyword import sec
from . import characters

def execute(dict_in):
	if not isinstance(dict_in,dict) :
		raise Exception("[ERROR] Input has no target code.")
	
	#print(yaml.dump(dict_in, default_flow_style=False))
	#print("")
	list_lines = _unpack(dict_in)
	return list_lines


def _unpack(block,dict_parent_def={}):
	if not isinstance(block,dict):
		raise Exception("[ERROR] Input has no target code.")
	
	#print(yaml.dump(block, default_flow_style=False))
	#print(yaml.dump(dict_parent_def, default_flow_style=False))
	
	### Get task information ###
	PresentTask = block[sec._task_]
	
	### Create dict of defined variables ###
	def_pwl = block[sec._define_]
	def_total = copy.deepcopy(dict_parent_def)
	def_total.update(def_pwl)
	def_total = _updateDictVals(def_total)
	
	### Get parameter list ###
	if PresentTask in [ key._generator_, key._listup_ ] :
		parameters = block[sec._parameter_]
		parameters = _updateDictVals(parameters,def_total)
		
		if len(parameters) == 0 :
			list_paramset = [ {} ]
		
		else :
			### Create list for single value parameters ###
			for key_in,val in parameters.items():
				if not isinstance(val,list):
					parameters[key_in] = [val]
				continue
			
			### Create new dictionary set of parameters with parent parameters ###
			ptuple = [x for x in itertools.product(*parameters.values())]
			#list_paramset  = [dict(zip(parameters.keys(), r)) for r in ptuple]
			list_paramset = []
			for i,pset in enumerate(ptuple):
				dict_t = dict(zip(parameters.keys(),pset))
				list_paramset.append(dict_t)
				continue
		
	else :
		list_paramset = [ {} ]
		pass 
		
	
	### Create listup information ###
	if PresentTask == key._listup_ : 
		### Get format and listup position ###
		# Get format
		for key_d in def_pwl.keys():
			if re.match(r"\@\{.*\}",key_d):
				key_at = key_d
				break
			continue
		form = def_total[key_at]
		
		# Get delimiter
		for key_d in def_pwl.keys():
			if re.match(r"\#\{.*\}",key_d):
				key_del = key_d
				break
			continue
		delimiter = def_total[key_del]
		
		# List up elements
		list_t = []
		for i,pset in enumerate(list_paramset) :
			def_now = copy.deepcopy(def_total)
			def_now.update(pset)
			elem = _replace(form,def_now)
			list_t.append(elem)
			continue
		
		# Build listup text
		def_total[key_at] = delimiter.join(list_t)
		
		# Update def_total 
		def_total = _updateDictVals(def_total)
		
		### Create empty parameter ###
		list_paramset = [ {} ]
	else :
		pass
	
	
	#print(yaml.dump(def_total, default_flow_style=False))
	
	### Produce line by replace with parameters and definitions ###
	code_out = []
	for dict_param in list_paramset:
		dict_val_all = copy.deepcopy(def_total)
		dict_val_all.update(dict_param)
		if PresentTask in [ key._if_ ]:
			codes0 = block[sec._source_]
			codes = []
			for i,ifblock in enumerate(codes0):
				isMatch = False
				key_in = ifblock[0]
				if key_in == key._else_ :
					isMatch = True
				else:
					condition = _replace(ifblock[1],dict_val_all,isWrapCharacter=True)
					isMatch = eval(condition)
					#print(condition, isMatch)
				
				if isMatch :
					if ifblock[2] is None:
						codes = []
					else :
						codes = ifblock[2:]
					break
				continue
				
		else :
			codes = block[sec._source_]
		
		for i,code in enumerate(codes) :
			#print(code)
			if code is None:
				continue
			elif isinstance(code,dict):
				lines = _unpack(code,dict_val_all)
				code_out.extend(lines)
			else:
				line = _replace(code,dict_val_all)
				code_out.append(line)
			continue
		continue
	
	return code_out


def _replace(line0,dict_val,isWrapCharacter=False,PermitUndefVals=False):
	#####
	def __get(name0,dict_val):
		### Text check ###
		if name0 is None:
			raise Exception("[ERROR] Input text is None-Type object!")
		
		elif len(name0) == 0 :
			return name0
		
		else :
			pass
		
		name = name0
		### Dictionary check ###
		if not isinstance(dict_val, dict):
			raise Exception("[ERROR] Input dictionary is not a dictionary")
		
		### Replace ${*} from dictionary ###
		#--- List up all variables without duplicates ---
		list_vals    = [ i[2:-1] for i in characters.cliping(name0,"${","}")]
		list_vals    = list( dict.fromkeys(list_vals) )
		list_vals_at = characters.cliping(name0,"@{","}")
		
		#--- Replace ---
		for i,val0 in enumerate(list_vals):
			val = __get(val0,dict_val)
			val_rep ="${"+val0+"}"
			name = name.replace(val_rep,str(val))
			continue
		
		### Replace @{*} from dictionary ###
		# Not needed ...
		
		### Replace list or dictionary defined variable ###
		if re.match(r".*\[.*\]",name):
			#--- Get key name and dictionary ---
			key_in = re.match(r".*\[",name).group()
			key_in = key_in[0:-1]
			dict_def = dict_val.get(key_in)
			
			#--- List up all arguments ---
			args = [ i[1:-1] for i in characters.cliping(name,"[","]")]
			
			#--- Get value ---
			if len(args) == 1:
				name = dict_def[args[0]]
			elif len(args) == 2:
				name = dict_def[args[0]][args[1]]
			elif len(args) == 3:
				name = dict_def[args[0]][args[1]][args[2]]
			elif len(args) > 3 :
				raise Exception("[ERROR] Too many arguments! ["+args+"]")
			else:
				raise Exception("[ERROR] Illegal arguments! ["+args+"]")
			
		else:
			name1 = name
			name = dict_val.get(name1)
			if name is None :
				if PermitUndefVals:
					name = "${" + name1 + "}"
				else :
					raise Exception("[ERROR] Undefined variable is detected! ["+name0+"]")
			
		return name
	#####
	
	if len(dict_val.keys()) == 0 :
		return line0
	
	line = copy.deepcopy(line0)
	list_vals    = [ i[2:-1] for  i in characters.cliping(line0,"${","}")]
	list_vals_at = characters.cliping(line0,"@{","}")
	list_vals.extend(list_vals_at)
	list_vals    = list( dict.fromkeys(list_vals) )
	for i,val0 in enumerate(list_vals):
		val = __get(val0,dict_val)
		if isWrapCharacter and isinstance(val,str):
			val = "".join(["'",str(val),"'"])
		else:
			val = str(val)
		
		if re.match(r"\@\{.*\}",val0):
			val_rep = val0
		else :
			val_rep = "${"+val0+"}"
		
		line = line.replace(val_rep,val)
		for j,jval in enumerate(list_vals[i+1:]):
			jval = jval.replace(val_rep,val)
			list_vals[i+1+j] = jval
			continue
		continue
	
	return line


def _updateDictVals(dict1,dict2={}):
	dict_tot = copy.deepcopy(dict1)
	dict_tot.update(dict2)
	
	dict_out = copy.deepcopy(dict1)
	for key,val in dict_out.items():
		val_t = str(val)
		if re.search(re.escape("${") + ".+" + re.escape("}"),val_t) :
			val_str = _replace(val_t,dict_tot,PermitUndefVals=True)
			if re.search(re.escape("${") + ".+" + re.escape("}"),val_str) :
				dict_out[key] = val_str
			else :
				val_after = eval(val_str)
				if type(val_after) == type(range(1)) :
					val_after = [ i for i in val_after ]
				else :
					pass
				dict_out[key] = val_after
		else:
			continue
		continue
	
	return dict_out












