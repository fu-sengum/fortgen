###
### Code generator
### 
import os
import sys
import re
import pathlib

###
### Definitions
###
from . import parse
from . import generate
from . import transform

###
### Main
###

def execute(filename,output=None):
	inputfile = pathlib.Path(filename.strip()).resolve()
	if inputfile.exists():
		pass
	else:
		raise Exception("[ERROR] Target source file is not find. ["+str(inputfile)+"]")
	
	#=== Set output file name ===
	inputname = os.path.basename(inputfile)
	inputbody, inputext = os.path.splitext(inputname)
	
	inputpath = os.path.dirname(inputfile)
	
	if output is None:
		outputfile = pathlib.Path(inputpath + "/" + inputbody + "__genmod.f90")
	elif type(output) == type(pathlib.Path("")):
		outputfile = output
	else :
		outputfile = pathlib.Path(output).resolve()
	
	#=== Decompose dictlist format ===#
	list_dict = parse.execute(inputfile) 
	
	#=== Reform dictlist for "parameterize" valiables and additional information ===#
	# Distribute valiable #
	# Judge valiable, subroutine, function, module, class generator for naming #
	# #
	list_dict = transform.reflectImport(list_dict,inputpath)
	list_dict = transform.Levelup_DefAndParam(list_dict)
	
	#=== Create source code ===
	list_lines = generate.execute(list_dict)
	
	f_out = open(outputfile,mode="w")
	f_out.write("\n".join(list_lines))
	f_out.close()
	
	return outputfile










