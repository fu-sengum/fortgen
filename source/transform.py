###
###
###
import pathlib
import yaml

###
###
###
from .define_keyword import key
from .define_keyword import sec 


def Levelup_DefAndParam(dict_in):
	if not isinstance(dict_in,dict) :
		raise Exception("[ERROR] Input has no target code.")
	
	def _recursive(dict_in):
		list_source_out = []
		list_source = dict_in[sec._source_]
		for i,block in enumerate(list_source):
			if isinstance(block,dict):
				task = block[sec._task_]
				if task  == key._define_:
					dict_in[sec._define_].update(block[sec._define_])
				
				elif task == key._parameterize_ :
					dict_in[sec._parameter_].update(block[sec._parameter_])
				
				elif task == key._import_ :
					dict_in[sec._define_].update(block[sec._define_])
					dict_in[sec._parameter_].update(block[sec._parameter_])
				
				else:
					dict_t = _recursive(block)
					list_source_out.append(dict_t)
			
			else :
				list_source_out.append(block)
			
			continue
		
		dict_in[sec._source_] = list_source_out
		return dict_in
	
	
	dict_out = _recursive(dict_in)
	return dict_out


def reflectImport(dict_in,pwd="."):
	if not isinstance(dict_in,dict) :
		raise Exception("[ERROR] Input has no target code.")
	
	def _recursive(dict_in):
		task = dict_in.get(sec._task_)
		if task == key._import_ :
			list_source = dict_in[sec._source_]
			for i,filename in enumerate(list_source) :
				target = pathlib.Path(filename)
				if target.exists() :
					pass
				else:
					target = pathlib.Path(str(pwd) + "/" + filename).resolve()
					if target.exists() :
						pass
					else:
						raise Exception("[ERROR] No such file to be imported. [" + str(filename) + " in " + str(pwd) + "]")
				
				f_in = open(target)
				dict_t = yaml.safe_load(f_in)
				f_in.close()
				dict_t = _normalize(dict_t)
				
				# Reflect "define" section #
				dict_t_define = dict_t.get(sec._define_)
				if dict_t_define is not None:
					dict_in[sec._define_].update(dict_t_define)
				
				# Reflect "parametrize" section #
				dict_t_parameter = dict_t.get(sec._parameter_)
				if dict_t_parameter is not None:
					dict_in[sec._parameter_].update(dict_t_parameter)
				
				continue
			
			list_source_out = []
		
		else :
			list_source_out = []
			list_source = dict_in[sec._source_]
			for i,block in enumerate(list_source):
				if isinstance(block,dict):
					dict_t = _recursive(block)
					list_source_out.append(dict_t)
				
				else :
					list_source_out.append(block)
				
				continue
		
		dict_in[sec._source_] = list_source_out
		return dict_in
	
	
	dict_out = _recursive(dict_in)
	return dict_out


#####
def _normalize(dict_in):
	if not isinstance(dict_in, dict):
		raise Exception("[ERROR] Invalid input type. [" + str(type(dict_in)) + "]")
	
	dict_out = {} #copy.deepcopy(key.template)
	for key_in,value in dict_in.items():
		key_new = key.toSystemKeyword(key_in)
		dict_out[key_new] = value
		continue
	
	return dict_out

