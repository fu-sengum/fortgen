
###
###
###
import re
import sys
import copy

###
###
###

def get(line,list_keyword0):
	if not isinstance(line,str):
		raise Exception("[ERROR] Cannot treat such type of input. ["+type(line)+"]")
	
	if not isinstance(list_keyword0,list):
		raise Exception("[ERROR] Keyword must be given in list object.")
	else:
		list_keyword = [ i.upper() for i in list_keyword0 ] 
		
	dict_out = {
		"*" : None,
	}
	
	### Create block ###
	args0 = re.split(r"[ ]",line)
	list_block = []
	list_t = ["*"]
	for arg in args0:
		if arg.upper() in list_keyword :
			if len(list_t) == 0 :
				pass
			else :
				list_block.append(list_t)
			
			list_t = []
		
		list_t.append(arg)
		if arg == args0[-1] : 
			list_block.append(list_t)
		continue
	
	### Translate to dictionary ###
	for item in list_block :
		keyword = item[0].lower()
		dict_out[keyword] = " ".join(item[1:]).strip() 
		continue
	
	return dict_out 





