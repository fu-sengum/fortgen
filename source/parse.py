###
###
###
import re
import os
import sys
import copy
import pathlib
import yaml

###
###
###
from .define_keyword import key
from .define_keyword import sec


import parsers._import_
import parsers._define_
import parsers._parameterize_
import parsers._generator_
import parsers._if_
import parsers._elseif_
import parsers._else_
import parsers._switch_
import parsers._case_
import parsers._do_
import parsers._listup_

pwd = pathlib.Path(".").resolve()

###
def execute(data_in):
	if isinstance(data_in,list):
		lines = data_in
	
	elif isinstance(data_in,str):
		if "\n" in data_in:
			lines = re.split("\n",data_in)
		else:
			try:
				f_in = open(data_in)
			except FileNotFoundError:
				lines = [ data_in ]
			except:
				raise Exception("[ERROR] Unknown error in parse.execute!")
			else:
				pwd = pathlib.Path(os.path.dirname(data_in)).resolve()
				lines = f_in.readlines() 
				f_in.close()
		
	elif type(data_in) == type(pathlib.Path("dum.txt")) :
		try:
			f_in = open(data_in)
		except FileNotFoundError:
			raise Exception("[ERROR] File not found! [" + str(data_in) + "]")
		else:
			pwd = pathlib.Path(os.path.dirname(data_in)).resolve()
			lines = f_in.readlines() 
			f_in.close()
		
	else:
		raise Exception("[ERROR] Invalid type is found. [" + str(type(data_in)) + "]")
	
	# Remove "\n"
	lines = [ i.replace("\n","") for i in lines ]
	
	# Make list blocks
	lines_mod = _toListInList(lines)
	#print(yaml.dump(lines_mod, default_flow_style=False))
	
	dict_out = copy.deepcopy(sec.template)
	dict_out[sec._task_] = "root"
	for block in lines_mod:
		if isinstance(block,list):
			dict_t = _toDict(block)
			dict_out[sec._source_].append(dict_t)
		else:
			dict_out[sec._source_].append(block)
		continue
	
	return dict_out

#####
def _toDict(lines):
	# [!$fcg start ... , ..., !$fcg end ...]
	dict_out = copy.deepcopy(sec.template)
	PresentSection = None
	
	#--- Judge the first line ---
	line = lines[0]
	args = [ i.strip() for i in re.split(r"[ ]+",line.strip()) if len(i) > 0 ] 
	if re.match(re.escape(key.prefix), line.strip(), flags=re.IGNORECASE) :
		del args[0]
		todo = key.toSystemKeyword(args[0])
		if todo is None :
			raise Exception("[ERROR] Unknown keyword is detected. [" + args[0] + "]")
		elif todo in [ key._import_, key._define_, key._parameterize_ ] :
			if len(args) == 1 :
				args.insert(0,key._start_)
				todo = key._start_
		
		elif todo in [ key._if_, key._switch_, key._generator_, key._do_, key._listup_ ] :
			args.insert(0,key._start_)
			todo = key._start_
		
		if todo == key._start_:
			del args[0]
			
			PresentSection = key.toSystemKeyword(args[0])
			if PresentSection is None:
				raise Exception("[ERROR] Unknown keyword is detected. [" + args[0] + "]")
			
			dict_out[sec._task_] = PresentSection
			
			del args[0]
			if PresentSection == key._generator_ :
				wrk = parsers._generator_.wrk()
			
			elif PresentSection == key._import_ :
				wrk = parsers._import_.wrk()
			
			elif PresentSection == key._define_ :
				wrk = parsers._define_.wrk()
			
			elif PresentSection == key._parameterize_ :
				wrk = parsers._parameterize_.wrk()
			
			elif PresentSection == key._do_ :
				wrk = parsers._do_.wrk(" ".join(args))
			
			elif PresentSection == key._listup_ :
				wrk = parsers._listup_.wrk(" ".join(args))
			
			elif PresentSection == key._if_ :
				wrk = parsers._if_.wrk(" ".join(args))
			
			elif PresentSection == key._switch_ :
				target = " ".join(args)
				#parsers._switch_.wrk wrk(" ".join(args))
				wrk = None
				dict_out[sec._task_] = key._if_
			
			else :
				raise Exception("[ERROR] Inappropriate block is detected. [" + PresentSection + "]")
			
		else :
			# --- Inline type format ---
			dict_out[sec._task_] = todo 
			del args[0]
			if todo == key._import_ :
				dict_out = parsers._import_.inline(dict_out," ".join(args))
					
			elif todo == key._define_ :
				dict_out = parsers._define_.inline(dict_out," ".join(args))
				
			elif todo == key._parameterize_ :
				dict_out = parsers._parameterize_.inline(dict_out," ".join(args))
			
			else :
				raise Exception("[ERROR] Unknown keyword is detected. [" + args[0] + "]")
			
			return dict_out
	
	else:
		print(lines)
		raise Exception("[ERROR] List-block structure is something wrong.")
	
	#--- To dictionary for other lines ---
	del lines[0]  # Delete "start" line
	del lines[-1] # Delete "end" line
	
	for iblock,block in enumerate(lines):
		if isinstance(block,list):
			dict_t = _toDict(block)
			
			
			todo = dict_t[sec._task_]
			if todo == key._define_ :
				dict_out[sec._define_].update(dict_t[sec._define_])
			
			elif todo == key._parameterize_ :
				dict_out[sec._parameter_].update(dict_t[sec._parameter_])
			
			elif todo == key._import_ :
				#dict_out[sec._define_].update(dict_t[sec._define_])
				#dict_out[sec._parameter_].update(dict_t[sec._parameter_])
				pass

			elif PresentSection == key._listup_ :
				wrk.input(dict_t)
			
			elif PresentSection == key._do_ :
				wrk.input(dict_t)
			
			elif PresentSection == key._generator_ :
				wrk.input(dict_out,dict_t)
			
			elif PresentSection == key._if_ :
				wrk.input(dict_t)
			
			elif  PresentSection == key._switch_ :
				if wrk is not None:
					wrk.input(dict_t)
			
			else :
				dict_out[sec._source_].append(dict_t)
		
		else :
			### Plain text ###
			args = re.split(r"[ ]+",block.strip()) 
				
			if re.match(re.escape(key.prefix), block.strip(), flags=re.IGNORECASE) :
				del args[0]
				todo = key.toSystemKeyword(args[0])
				
				### Directive line ###
				if len(args) == 0 :
					continue
					
				elif todo == key._elif_ :
					dict_out = wrk.finalize(dict_out)
					del wrk
					del args[0]
					wrk = parsers._elseif_.wrk(" ".join(args))
					
				elif todo == key._else_ :
					dict_out = wrk.finalize(dict_out)
					del wrk
					del args[0]
					wrk = parsers._else_.wrk(" ".join(args))
				
				elif todo == key._case_ :
					if wrk is not None :
						dict_out = wrk.finalize(dict_out)
					del args[0]
					wrk = parsers._case_.wrk(target," ".join(args))
						
				else :
					pass #raise Exception("[ERROR] Unknown keyword is detected. [" + args[0] + "]")
			
			else:
				### None directive line ###
				if PresentSection is None :
					#???
					print(block)
					raise Exception()
					
				elif PresentSection == key._generator_ :
					dict_out = wrk.input(dict_out,block)
				
				elif PresentSection == key._if_ :
					wrk.input(block)
				
				elif PresentSection == key._do_ :
					wrk.input(block)
				
				elif PresentSection == key._listup_ :
					wrk.input(block)
				
				elif PresentSection == key._switch_ :
					if wrk is not None:
						wrk.input(block)
					
				elif PresentSection == key._import_ :
					wrk.input(block)
				
				elif PresentSection == key._define_ :
					wrk.input(block)
					
				elif PresentSection == key._parameterize_  :
					wrk.input(block)
					
				else :
					raise Exception("[ERROR] Unknown section is detected. [" + PresentSection + "]")
		continue
	
	dict_out = wrk.finalize(dict_out)
	del wrk
	return dict_out



#####
def _toListInList(lines):
	if not isinstance(lines,list):
		raise Exception("[ERROR] The argument must be list-type object.")
	
	lines_mod = [" ".join([key.prefix,key._start_,"root"])]
	lines_mod.extend(lines)
	lines_mod.append(" ".join([key.prefix,key._end_,"root"]))
	
	lines_out = _toList(lines_mod)
	del lines_out[0]
	del lines_out[-1]
	return lines_out

#####
def _toList(lines):
	lines_mod = []
	lines_lower = []
	labels = []
	
	for i,line in enumerate(lines):
		args = [ i.strip() for i in re.split(r"[ ]+",line.strip()) if len(i) > 0 ]
		todo = None
		
		# Determine layer #
		if re.match(re.escape(key.prefix), line.strip(), flags=re.IGNORECASE) :
			del args[0]
			if len(args) == 0 :
				continue
			
			todo = key.toSystemKeyword(args[0])
			if todo is None:
				raise Exception("[ERROR] Unknown keyword is detected. [" + args[0] + "]")
			
			elif todo == key._end_ :
				pass
			
			elif todo == key._start_:
				del args[0]
				if len(args) == 0 :
					raise Exception("[ERROR] No keywords after 'start' directive.")
				else :
					todo = key.toSystemKeyword(args[0])
					labels.append(todo)
				
			elif todo in [ key._if_, key._switch_ ] :
				labels.append(todo)
			
			elif todo in [ key._import_, key._define_, key._parameterize_ ] :
				if len(args) >= 2 :
					# For inline-type format
					labels.append(todo)
					todo = key._end_
				elif len(args) == 1 :
					# For multi-line-type format
					labels.append(todo)
				else :
					raise Exception("[ERROR] Cannot analyze arguments.[" + str(args) + "]")
				
			elif todo in [ key._do_, key._listup_, key._generator_] :
				labels.append(todo)
			
			elif todo in key._toKeyword.values() :
				pass
			else : 
				raise Exception("[ERROR] Unknown keyword is found. [" + todo + "]")
			
		else :
			pass
		
		# Store lines #
		if len(labels) > 1 :
			lines_lower.append(line)
		else:
			lines_mod.append(line)
		
		# When end keyword is found #
		if todo is None :
			pass
		elif todo == key._end_ : # re.match(re.escape(key.prefix), line, flags=re.IGNORECASE) :
			if len(labels) == 0 :
				raise Exception("[ERROR] Too many 'end' directives are detected.")
				
			del labels[-1]
			if len(labels) == 1:
				lines_t = _toList(lines_lower)
				lines_mod.append(lines_t)
				lines_lower = []
			
			else :
				pass
			
		continue
	
	return lines_mod







