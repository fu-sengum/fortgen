###
###
###


###
###
###


class key:
	prefix = "!$fcg"
	
	_start_ = "start"
	_end_   = "end"
	
	_import_       = "import"
	_generator_    = "generator"
	_define_       = "define"
	_parameterize_ = "parameterize"
	_do_           = "do"
	
	_if_   = "if"
	_elif_ = "elif"
	_else_ = "else"
	_endif_ = "endif"
	
	_switch_  = "switch"
	_case_    = "case"
	_default_ = "default"
	
	_listup_ = "listup"
	_at_     = "at"
	_delimiter_ = "delimiter"
	_format_ = "format"
	
	_toKeyword = {
		"START" : _start_,
		"BEGIN" : _start_,
		"GO"    : _start_,
		
		"END"    : _end_,
		"FINISH" : _end_,
		
		"IMPORTS" : _import_,
		"IMPORT"  : _import_,
		"INCLUDES": _import_,
		"INCLUDE" : _import_,
		
		"GENERATORS" : _generator_,
		"GENERATOR"  : _generator_,
		"GENERATE"   : _generator_,
		"GEN"        : _generator_,
		
		"DEF"         : _define_,
		"DEFS"        : _define_,
		"DEFINE"      : _define_,
		"DEFINES"     : _define_,
		"DEFINITIONS" : _define_,
		"DEFINITION"  : _define_,
		
		"PARAM"         : _parameterize_,
		"PARAMS"        : _parameterize_,
		"PARAMETER"     : _parameterize_,
		"PARAMETERS"    : _parameterize_,
		"PARAMETERIZE"  : _parameterize_,
		"PARAMETERIZES" : _parameterize_,
		"PARAMETRIZE"   : _parameterize_,
		"PARAMETRIZES"  : _parameterize_,
		
		"DO"     : _do_,
		"FOR"    : _do_,
		"LOOP"   : _do_,
		"WHILE"  : _do_,
		"ENDDO"  : _end_,
		"ENDFOR" : _end_,
		
		"IF"     : _if_,
		"ELSEIF" : _elif_,
		"ELIF"   : _elif_,
		"ELSE"   : _else_,
		"ENDIF"  : _end_,
		
		"SELECT"  : _switch_,
		"SWITCH"  : _switch_,
		"CASE"    : _case_,
		"WHEN"    : _case_,
		"DEFAULT" : _default_,
		"OTHER"   : _default_,
		
		"LISTUP"  : _listup_,
		"LIST_UP" : _listup_,
		"LIST-UP" : _listup_,
		"LIST"    : _listup_,
		"ITEMIZE" : _listup_,
		"ITEM"    : _listup_,
		"AT"      : _at_,
		"DELIMITER" : _delimiter_,
		"SEPARATER" : _delimiter_,
		"CONNECTER" : _delimiter_,
		"FORMAT" : _format_,
		"FORM"   : _format_,
	}
	
	@classmethod
	def toSystemKeyword(cls,key_in):
		if not isinstance(key_in,str) :
			raise Exception("[ERROR] Invalid input type!")
		key_upper = key_in.upper()
		key = cls._toKeyword.get(key_upper)
		return key



class sec :
	_task_        = "task"
	_define_      = key._define_ 
	_parameter_   = key._parameterize_ 
	_source_      = "source"

	template = {
		_task_      : None,  # Keyword [import,generator,generators]
		_define_    : {},    # Internal valiable definition
		_parameter_ : {},    # Generation parameter
		_source_    : [],    # Code template
	}










