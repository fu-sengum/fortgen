import sys
import pathlib

pwd = pathlib.Path(__file__).resolve().parent
pwd_p = pwd.parent
sys.path.append(str(pwd))
sys.path.append(str(pwd_p))

