###
###
###
import re
import sys
import copy
import pathlib
import yaml

###
###
###
from define_keyword import key
from define_keyword import sec
from template import template


class wrk(template) :
	_name_keyword = key._parameterize_
	
	def __init__(self):
		self._storage = []
		return
	
	def input(self,line):
		self._storage.append(line)
		return
	
	def finalize(self,dict_out):
		text_yaml = "\n".join(self._storage)
		dictlist_t = yaml.safe_load(text_yaml)
		if isinstance(dictlist_t,dict):
			if len(dictlist_t) > 0 :
				dict_out[sec._parameter_].update(dictlist_t)
		else:
			raise Exception("\n".join(["Unavailable format of yaml description. ",str(dictlist_t)]))
		return dict_out
	
	def __del__(self):
		del self._storage
		return
	

#####
def inline(dict_out,line):
	# FORMAT: i = [1,2,3,4]
	# FORMAT: T = ["integer(4)","integer(8)"]
	# FORMAT: j = range(1,3,15)
	# FORMAT: i in [1,2,3,4]
	args = re.split(r"[ ]+",line)
	if "in" == args[1].strip() :
		separator = " in "
	else :
		separator = "="
	valname = args[0].strip()
	del args
	
	val_str = re.sub(valname + "[ ]*" + re.escape(separator) + "[ ]*", "",line)
	if re.search(re.escape("${") + ".+" + re.escape("}"),val_str):
		# For use of defined variables
		vals = val_str
	else :
		vals = eval(val_str)
		if type(vals) == type(range(0,1)) :
			vals = [ i for i in vals ]
	
	dict_t = { valname : vals }
	dict_out[sec._parameter_].update(dict_t)
	return dict_out



