###
###
###
import re
import sys
import copy
import pathlib
import yaml

###
###
###
from define_keyword import key
from define_keyword import sec
from template import template


class wrk(template):
	_name_keyword = key._if_
	
	def __init__(self,line0):
		line = " ".join(re.split(r"[ ]+",line0))
		self._storage = [key._if_,line.strip()]
		return
	
	def input(self,line):
		self._storage.append(line)
		return
	
	def finalize(self,dict_out):
		if len(self._storage) == 2:
			self._storage.append(None)
		dict_out[sec._source_].append(self._storage)
		return dict_out
	
	def __del__(self):
		del self._storage
		return
	


