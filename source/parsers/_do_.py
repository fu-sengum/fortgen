###
###
###
import re
import sys
import copy
import pathlib

###
###
###
from define_keyword import key
from define_keyword import sec
from template import template
import parsers._parameterize_


class wrk(template):
	_name_keyword = key._do_
	
	def __init__(self,line0):
		args = re.split(r"[ ]+", line0)
		args[1] = " = "
		
		self._parameter = " ".join(args)
		self._storage = []
		return
	
	def input(self,line):
		self._storage.append(line)
		return
	
	def finalize(self,dict_out):
		dict_out = parsers._parameterize_.inline(dict_out,self._parameter)
		dict_out[sec._task_] = key._generator_
		dict_out[sec._source_].extend(self._storage)
		return dict_out
	
	def __del__(self):
		del self._parameter
		del self._storage
		return
	


