###
###
###
import re
import sys
import copy
import pathlib
import yaml

###
###
###
from define_keyword import key
from define_keyword import sec
from template import template


class wrk(template) :
	_name_keyword = key._switch_
	
	def __init__(self,line):
		self._target = line
		return
	
	def input(self,line):
		raise Exception("[ERROR] Statement 'switch' does not have input operation.")
		return
	
	def finalize(self,dict_out):
		return dict_out
	
	def __del__(self):
		return


