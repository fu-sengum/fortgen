###
###
###
import re
import sys
import copy
import pathlib
import yaml

###
###
###
from define_keyword import key
from define_keyword import sec
from template import template


class wrk(template) :
	_name_keyword = key._import_
	
	def __init__(self,line=None):
		self._storage = []
		
		if line is not None:
			self.input(line)
		
		return
	
	def input(self,line):
		if len(line.strip()) == 0 :
			return
		
		self._storage.append(line)
		return
	
	def finalize(self,dict_out):
		text_yaml = "\n".join(self._storage)
		dictlist_t = yaml.safe_load(text_yaml)
		for filename in dictlist_t :
			dict_out[sec._source_].append(filename)
			continue
		return dict_out
	
	def __del__(self):
		del self._storage
		return
	
#####
def inline(dict_out,line):
	wrk_t = wrk()
	
	files = [ "- " + i for i in re.split(r"[ ]+",line.strip()) if len(i) > 0 ] 
	for filename in files:
		wrk_t.input( filename )
		continue
	
	dict_out = wrk_t.finalize(dict_out)
	return dict_out



















