###
### Loading default python modules
###
import os
import sys
import pytest

from source import argument 

class Test_argument():
	pattern_get = []
	pattern_get.append(
		(
			"format i${i} delimiter + at here for i in ${range}"
			,
			["delimiter","at","for","format"]
			,
			{
				"format" : "i${i}",
				"delimiter" : "+",
				"at" : "here",
				"for" : "i in ${range}",
				"*" : "",
			}
		)
	)
	pattern_get.append(
		(
			"delimiter + at here for i in ${range}"
			,
			[]
			,
			{
				"*" : "delimiter + at here for i in ${range}",
			}
		)
	)
	
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	#####
	#####
	@pytest.mark.parametrize("data_in,keywords,expected",pattern_get)
	def test_get(self,data_in,keywords,expected):
		result = argument.get(data_in,keywords)
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		return








