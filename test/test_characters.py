###
### Loading default python modules
###
import os
import sys
import pathlib
import pytest

from source import characters 

class Test_characters():
	pattern_cliping = []
	pattern_cliping.append(
		(
			"(doasgie(dis(),srus)skfu)"
			,
			"(",")"
			,
			["(doasgie(dis(),srus)skfu)"]
		)
	)
	pattern_cliping.append(
		(
			"doasgie(dis(),srus)skfu(dis())"
			,
			"(",")"
			,
			["(dis(),srus)","(dis())"]
		)
	)
	pattern_cliping.append(
		(
			"doasgie$(dis$(),srus)skfu$(dis$())"
			,
			"$(",")"
			,
			["$(dis$(),srus)","$(dis$())"]
		)
	)
	
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	#####
	#####
	@pytest.mark.parametrize("data_in,del1,del2,expected",pattern_cliping)
	def test_cliping(self,data_in,del1,del2,expected):
		result = characters.cliping(data_in,del1,del2)
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		return








