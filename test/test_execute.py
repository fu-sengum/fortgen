###
### Loading default python modules
###
import os
import sys
import pathlib
import pytest
import glob

from source import main 


def pytest_generate_tests(metafunc):
	pwd = pathlib.Path(__file__).resolve().parent
	pwd_p = pwd.parent
	files = glob.glob(str(pwd_p) + "/sample/*.f90")
	#print(files)
	metafunc.parametrize("file_in", files)
	return

class Test_main():
	#pattern_execute = files #glob.glob("../test/*.f90")
	
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	#####
	#####
	#@pytest.mark.parametrize("file_in",pattern_execute)
	def test_execute(self,file_in):
		file_out = main.execute(file_in)
		
		f = open(file_in + ".OK")
		expected = f.readlines() 
		f.close()
		expected[-1] = expected[-1].replace("\n","")
		
		f = open(file_out)
		result = f.readlines() 
		f.close()
		result[-1] = result[-1].replace("\n","")
		
		os.remove(file_out)
		
		print(file_in)
		print(result)
		print(expected)
		assert result == expected
		return








