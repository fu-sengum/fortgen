###
### Loading default python modules
###
import os
import sys
import pathlib
import pytest
import yaml

from source import parse 

class Test_parse():
	pattern_parse = []
	pattern_parse.append(
		(
			"\n".join([
				"!$fcg parameterize i = [1,2,3,4]",
			])
		,
			{
				"task" : "root",
				"define" : {},
				"parameterize" : {},
				"source" : [
					{
						"task" : "parameterize",
						"define" : {
						},
						"parameterize" : {
							"i" : [1,2,3,4]
						},
						"source" : [],
					},
				]
			},
		)
	)
	pattern_parse.append(
		(
			"\n".join([
				"!$fcg start parameterize", 
				"i : [1,2,3,4]",
				"j : [4,6,2]",
				"!$fcg end parameterize",
			])
		,
			{
				"task" : "root",
				"define" : {},
				"parameterize" : {},
				"source" : [
					{
						"task" : "parameterize",
						"parameterize" : {
							"i" : [1,2,3,4],
							"j" : [4,6,2],
						},
						"define" : {},
						"source" : [],
					},
				]
			},
		)
	)
	pattern_parse.append(
		(
			"\n".join([
				"!$fcg start parameterize", 
				"i : [1,2,3,4]",
				"j : [4,6,2]",
				"!$fcg end parameterize",
				"aaa",
				"!$fcg start parameterize", 
				"k : [1,2,3,4]",
				"l : [4,6,2]",
				"!$fcg end parameterize",
			])
		,
			{
				"task" : "root",
				"define" : {},
				"parameterize" : {},
				"source" : [
					{
						"task" : "parameterize",
						"parameterize" : {
							"i" : [1,2,3,4],
							"j" : [4,6,2],
						},
						"define" : {},
						"source" : [],
					},
					"aaa",
					{
						"task" : "parameterize",
						"parameterize" : {
							"k" : [1,2,3,4],
							"l" : [4,6,2],
						},
						"define" : {},
						"source" : [],
					},
				]
			},
		)
	)
	






	#####
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	
	@pytest.mark.parametrize("data_in,expected",pattern_parse)
	def test_execute(self,data_in,expected):
		result = parse.execute(data_in)
		print(data_in)
		print(yaml.dump(result, default_flow_style=False))
		print("")
		print(yaml.dump(expected, default_flow_style=False))
		assert result == expected
		return





