###
### Loading default python modules
###
import os
import sys
import pathlib
import pytest
import yaml

from source import parse 

class Test_parse():
	pattern_parse = []

	pattern_parse.append(
		(
			"\n".join([
				"!$fcg start listup format ${i} at HERE for i in [1,2,34,6]",
				" ${ii} a @{i}",
				"!$fcg end listup",
			])
			,
			{
				"task" : "root",
				"define" : {
				},
				"parameterize" : {},
				"source" : [
					{
						"task" : "listup",
						"define" : {
							"#{HERE}" : ",",
							"@{HERE}" : "${i}",
						},
						"parameterize" : {
							"i" : [1,2,34,6],
						},
						"source" : [
							" ${ii} a @{i}",
						]
					}
				],
			},
		)
	)
	
	pattern_parse.append(
		(
			"\n".join([
				"!$fcg start listup format ${i} at HERE1 for i in [1,2,34,6]",
				" ${ii} a @{HERE1}",
				"  !$fcg start listup format ${j} at HERE2 for j in [1,2,34,6]",
				"b @{HERE1}-@{HERE2}",
				"  !$fcg end listup",
				" ${ii} b @{HERE1}",
				"!$fcg end listup",
			])
			,
			{
				"task" : "root",
				"define" : {
				},
				"parameterize" : {},
				"source" : [
					{
						"task" : "listup",
						"define" : {
							"#{HERE1}" : ",",
							"@{HERE1}" : "${i}",
						},
						"parameterize" : {
							"i" : [1,2,34,6],
						},
						"source" : [
							" ${ii} a @{HERE1}",
							{
								"task" : "listup",
								"define" : {
									"#{HERE2}" : ",",
									"@{HERE2}" : "${j}",
								},
								"parameterize" : {
									"j" : [1,2,34,6],
								},
								"source" : [
									"b @{HERE1}-@{HERE2}",
								]
							},
							" ${ii} b @{HERE1}",
						]
					}
				],
			},
		)
	)

	pattern_parse.append(
		(
			"\n".join([
				"!$fcg start listup format ${i}${j} at HERE ",
				"!$fcg start parameterize",
				"i : [1,2,3,4]",
				"j : ['i','r','k','j']",
				"!$fcg end parameterize",
				" ${ii} a @{HERE}",
				"!$fcg end listup",
			])
			,
			{
				"task" : "root",
				"define" : {
				},
				"parameterize" : {},
				"source" : [
					{
						"task" : "listup",
						"define" : {
							"#{HERE}" : ",",
							"@{HERE}" : "${i}${j}",
						},
						"parameterize" : {
							"i" : [1,2,3,4],
							"j" : ['i','r','k','j'],
						},
						"source" : [
							" ${ii} a @{HERE}",
						]
					}
				],
			},
		)
	)




	#####
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	
	@pytest.mark.parametrize("data_in,expected",pattern_parse)
	def test_execute(self,data_in,expected):
		result = parse.execute(data_in)
		print(data_in)
		print(yaml.dump(result, default_flow_style=False))
		print("")
		print(yaml.dump(expected, default_flow_style=False))
		assert result == expected
		return





