###
### Loading default python modules
###
import os
import sys
import pathlib
import pytest
import yaml

from source import parse 

class Test_parse():
	pattern_parse = []
	pattern_parse.append(
		(
			"\n".join([
				"!$fcg define i_all = [1,2,3,4]",
			])
		,
			{
				"task" : "root",
				"define" : {},
				"parameterize" : {},
				"source" : [
					{
						"task" : "define",
						"define" : {
							"i_all" : [1,2,3,4]
						},
						"parameterize" : {},
						"source" : [],
					},
				]
			},
		)
	)
	pattern_parse.append(
		(
			"\n".join([
				"!$fcg start define", 
				"i : [1,2,3,4]",
				"j : [4,6,2]",
				"!$fcg end define",
			])
		,
			{
				"task" : "root",
				"define" : {},
				"parameterize" : {},
				"source" : [
					{
						"task" : "define",
						"define" : {
							"i" : [1,2,3,4],
							"j" : [4,6,2],
						},
						"parameterize" : {},
						"source" : [],
					},
				]
			},
		)
	)
	
	pattern_parse.append(
		(
			"\n".join([
				"!$fcg start define", 
				"i : [1,2,3,4]",
				"j : [4,6,2]",
				"!$fcg end define",
				"aaa",
				"!$fcg start define", 
				"k : [1,2,3,4]",
				"l : [4,6,2]",
				"!$fcg end define",
			])
		,
			{
				"task" : "root",
				"define" : {},
				"parameterize" : {},
				"source" : [
					{
						"task" : "define",
						"define" : {
							"i" : [1,2,3,4],
							"j" : [4,6,2],
						},
						"parameterize" : {},
						"source" : [],
					},
					"aaa",
					{
						"task" : "define",
						"define" : {
							"k" : [1,2,3,4],
							"l" : [4,6,2],
						},
						"parameterize" : {},
						"source" : [],
					},
				]
			},
		)
	)
	
	pattern_parse.append(
		(
			"\n".join([
				"!$fcg start define", 
				"i : [1,2,3,4]",
				"  !$fcg start define", 
				"j : [4,6,2]",
				"  !$fcg end define",
				"!$fcg end define",
			])
		,
			{
				"task" : "root",
				"define" : {},
				"parameterize" : {},
				"source" : [
					{
						"task" : "define",
						"define" : {
							"i" : [1,2,3,4],
							"j" : [4,6,2],
						},
						"parameterize" : {},
						"source" : [],
					},
				]
			},
		)
	)






	#####
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	
	@pytest.mark.parametrize("data_in,expected",pattern_parse)
	def test_execute(self,data_in,expected):
		result = parse.execute(data_in)
		print(data_in)
		print(yaml.dump(result, default_flow_style=False))
		print("")
		print(yaml.dump(expected, default_flow_style=False))
		assert result == expected
		return





