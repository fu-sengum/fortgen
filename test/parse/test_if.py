###
### Loading default python modules
###
import os
import sys
import pathlib
import pytest
import yaml

from source import parse 

class Test_parse():
	pattern_parse = []
	pattern_parse.append(
		(
			"\n".join([
				"  !$fcg if ${T} == 'integer'",
				"  i = 0",
				"  !$fcg elif ${T} == 'real'",
				"  i = 0.0e-00",
				"  !$fcg else",
				"  i = null",
				"  !$fcg end if",
			])
		,
			{
				"task" : "root",
				"define" : {},
				"parameterize" : {},
				"source" : [
					{
						"task" : "if",
						"define" : {},
						"parameterize" : {},
						"source" : [
							["if","${T} == 'integer'","  i = 0",],
							["elif","${T} == 'real'","  i = 0.0e-00",],
							["else",None,"  i = null",],
						],
					},
				],
			},
		)
	)
	pattern_parse.append(
		(
			"\n".join([
				"  !$fcg if ${T} == 'integer'",
				"    !$fcg if ${i} <0",
				"  i = 0",
				"    !$fcg else",
				"    !$fcg end if",
				"  !$fcg else",
				"  i = null",
				"  !$fcg end if",
			])
		,
			{
				"task" : "root",
				"define" : {},
				"parameterize" : {},
				"source" : [
					{
						"task" : "if",
						"define" : {},
						"parameterize" : {},
						"source" : [
							["if","${T} == 'integer'",
								{
									"task" : "if",
									"define" : {},
									"parameterize" : {},
									"source" : [ 
										[ "if", "${i} <0", "  i = 0" ],
										[ "else", None, None ],
									]
								}
							],
							["else",None,"  i = null"],
						],
					},
				],
			},
		)
	)






	#####
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	
	@pytest.mark.parametrize("data_in,expected",pattern_parse)
	def test_execute(self,data_in,expected):
		result = parse.execute(data_in)
		print(data_in)
		print(yaml.dump(result, default_flow_style=False))
		print("")
		print(yaml.dump(expected, default_flow_style=False))
		assert result == expected
		return





