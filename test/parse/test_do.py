###
### Loading default python modules
###
import os
import sys
import pathlib
import pytest
import yaml

from source import parse 

class Test_parse():
	pattern_parse = []
	
	pattern_parse.append(
		(
			"\n".join([
				"!$fcg start do i in [1,2,34,6]",
				" ${i} a",
				"!$fcg end do",
			])
			,
			{
				"task" : "root",
				"define" : {},
				"parameterize" : {},
				"source" : [
					{
						"task" : "generator",
						"define" : {},
						"parameterize" : {
							"i" : [1,2,34,6],
						},
						"source" : [
							" ${i} a",
						]
					}
				],
			},
		)
	)
	
	pattern_parse.append(
		(
			"\n".join([
				"!$fcg start do i in [1,2,34,6]",
				" ${i} a",
				"  !$fcg start do j in [1,2,34,6]",
				" ${j} b",
				"  !$fcg end do",
				" ${i} c",
				"!$fcg end do",
			])
			,
			{
				"task" : "root",
				"define" : {},
				"parameterize" : {},
				"source" : [
					{
						"task" : "generator",
						"define" : {},
						"parameterize" : {
							"i" : [1,2,34,6],
						},
						"source" : [
							" ${i} a",
							{
								"task" : "generator",
								"define" : {},
								"parameterize" : {
									"j" : [1,2,34,6],
								},
								"source" : [
									" ${j} b",
								]
							},
							" ${i} c",
						]
					}
				],
			},
		)
	)






	#####
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	
	@pytest.mark.parametrize("data_in,expected",pattern_parse)
	def test_execute(self,data_in,expected):
		result = parse.execute(data_in)
		print(data_in)
		print(yaml.dump(result, default_flow_style=False))
		print("")
		print(yaml.dump(expected, default_flow_style=False))
		assert result == expected
		return





