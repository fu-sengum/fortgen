###
### Loading default python modules
###
import os
import sys
import pathlib
import pytest
import yaml

from source import parse 

class Test_parse():
	pattern_parse = []
	
	pattern_parse.append(
		(
			"\n".join([
				"0",
				"!$fcg start generator",
				"a",
				"  !$fcg start generator",
				"b",
				"    !$fcg start generator",
				"c",
				"    !$fcg end generator",
				"d",
				"  !$fcg end generator",
				"e",
				"  !$fcg start generator",
				"f",
				"  !$fcg end generator",
				"g",
				"!$fcg end generator",
				"1",
			]),
			{
				"task" : "root",
				"define" : {},
				"parameterize" : {},
				"source" : [
					"0",
					{
						"task" : "generator",
						"define" : {},
						"parameterize" : {},
						"source" : [
							"a",
							{
								"task" : "generator",
								"define" : {},
								"parameterize" : {},
								"source" : [
									"b",
									{
										"task" : "generator",
										"define" : {},
										"parameterize" : {},
										"source" : [
											"c"
										],
									},
									"d",
								],
							},
							"e",
							{
								"task" : "generator",
								"define" : {},
								"parameterize" : {},
								"source" : [
									"f",
								],
							},
							"g",
						],
					},
					"1",
				],
			},
		)
	)






	#####
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	
	@pytest.mark.parametrize("data_in,expected",pattern_parse)
	def test_execute(self,data_in,expected):
		result = parse.execute(data_in)
		print(data_in)
		print(yaml.dump(result, default_flow_style=False))
		print("")
		print(yaml.dump(expected, default_flow_style=False))
		assert result == expected
		return





