###
### Loading default python modules
###
import os
import sys
import pathlib
import pytest
import yaml

from source import parse 

class Test_parse():
	pattern_parse = []
	pattern_parse.append(
		(
			"\n".join([
				"  !$fcg start switch  ${T}" ,
				"  !$fcg case 'integer'",
				"  i = 0",
				"  !$fcg case 'real'",
				"  i = 0.0e-00",
				"  !$fcg case default",
				"  i = null",
				"  !$fcg end switch",
			])
		,
			{
				"task" : "root",
				"define" : {},
				"parameterize" : {},
				"source" : [
					{
						"task" : "if",
						"define" : {},
						"parameterize" : {},
						"source" : [
							["elif","${T} == 'integer'","  i = 0",],
							["elif","${T} == 'real'","  i = 0.0e-00",],
							["else",None,"  i = null",],
						],
					},
				],
			},
		)
	)
	pattern_parse.append(
		(
			"\n".join([
				"0",
				"  !$fcg start switch  ${T}" ,
				"wrong",
				"  !$fcg case 'integer'",
				"  iiiiiii",
				"    !$fcg start switch ${b}",
				"    !$fcg case <0",
				"  i = 0",
				"    !$fcg case >0",
				"  i = ${b}",
				"    !$fcg case default",
				"  i = -1",
				"    !$fcg end switch",
				"  jjjjjjj",
				"  !$fcg case 'real'",
				"  i = 0.0e-00",
				"  !$fcg case default",
				"  i = null",
				"  !$fcg end switch",
				"1",
			])
		,
			{
				"task" : "root",
				"define" : {},
				"parameterize" : {},
				"source" : [
					"0",
					{
						"task" : "if",
						"define" : {},
						"parameterize" : {},
						"source" : [
							["elif","${T} == 'integer'", "  iiiiiii", {
								"task" : "if",
								"define" : {},
								"parameterize" : {},
								"source" : [
									["elif","${b} <0", "  i = 0",],
									["elif","${b} >0", "  i = ${b}",],
									["else",None, "  i = -1",],
								],
							}, "  jjjjjjj",],
							["elif","${T} == 'real'","  i = 0.0e-00",],
							["else",None,"  i = null",],
						],
					},
					"1",
				],
			},
		)
	)






	#####
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	
	@pytest.mark.parametrize("data_in,expected",pattern_parse)
	def test_execute(self,data_in,expected):
		result = parse.execute(data_in)
		print(data_in)
		print(yaml.dump(result, default_flow_style=False))
		print("")
		print(yaml.dump(expected, default_flow_style=False))
		assert result == expected
		return





