###
### Loading default python modules
###
import os
import sys
import pathlib
import pytest
import copy

from source import parse
import parsers._import_ 
from source.define_keyword import key
from source.define_keyword import sec

class Test_parsers_import():
	pattern_import = []
	pattern_import.append(
		(
			[
				"!$fcg import ../common/common.yml",
			]
			,
			{
				sec._task_ : key._import_,
				sec._define_: {},
				sec._parameter_ : {},
				sec._source_ : [
					"../common/common.yml",
				]
			}
		)
	)
	pattern_import.append(
		(
			[
				"!$fcg start import",
				"- ../common/common.yml",
				"- ../parent.yml",
				"- local.yml",
				"!$fcg end import",
			]
			,
			{
				sec._task_ : key._import_,
				sec._define_: {},
				sec._parameter_ : {},
				sec._source_ : [
					"../common/common.yml",
					"../parent.yml",
					"local.yml",
				],
			}
		)
	)

	#####
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	#####
	#####

	@pytest.mark.parametrize("data_in,expected",pattern_import)
	def test_parse(self,data_in,expected):
		result = parse._toDict(data_in)
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		return
	






