###
### Loading default python modules
###
import os
import sys
import pathlib
import pytest
import copy
import re

from source import parse
import parsers._parameterize_ 
from source.define_keyword import key
from source.define_keyword import sec 

class Test_parsers_parameterize():
	pattern_parameterize = []
	pattern_parameterize.append(
		(
			[
				"!$fcg start parameterize",
				"x : [4.0e-001]",
				"i : [1,2,3,4]",
				'T : ["integer4","integer8"]',
				'u : ${def_u}',
				'str : ["integer(kind=4)","integer(kind=8)"]',
				"!$fcg end parameterize",
			]
			,
			{
				"x" : [4.0e-001],
				"i" : [1,2,3,4],
				"T" : ["integer4","integer8"],
				'u' : '${def_u}',
				"str" : ["integer(kind=4)","integer(kind=8)"],
			}
		)
	)

	pattern_parameterize_inline = []
	pattern_parameterize_inline.append(
		(
			"!$fcg parameterize x = [4.0e-001]",
			{
				"x" : [4.0e-001],
			}
		)
	)
	pattern_parameterize_inline.append(
		(
			"!$fcg parameterize i = [1,2,3,4]",
			{
				"i" : [1,2,3,4],
			}
		)
	)
	pattern_parameterize_inline.append(
		(
			'!$fcg parameterize T = ["integer4","integer8"]',
			{
				"T" : ["integer4","integer8"],
			}
		)
	)
	pattern_parameterize_inline.append(
		(
			'!$fcg parameterize str = ["integer(kind=4)","integer(kind=8)"]',
			{
				"str" : ["integer(kind=4)","integer(kind=8)"],
			}
		)
	)
	pattern_parameterize_inline.append(
		(
			"!$fcg parameterize x = range(1,10,3)",
			{
				"x" : [1,4,7],
			}
		)
	)
	pattern_parameterize_inline.append(
		(
			"!$fcg parameterize i = ${list_i}",
			{
				"i" : "${list_i}",
			}
		)
	)



	#####
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	#####
	#####
	@pytest.mark.parametrize("data_in,expected",pattern_parameterize)
	def test_parameterize(self,data_in,expected):
		dict_out = copy.deepcopy(sec.template)
		wrk = parsers._parameterize_.wrk()
		for data in data_in[1:-1] :
			wrk.input(data)
			continue
		dict_out = wrk.finalize(dict_out)
		result = dict_out[sec._parameter_]
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		del wrk
		del dict_out
		return

	@pytest.mark.parametrize("data_in,expected",pattern_parameterize)
	def test_parse(self,data_in,expected):
		dict_out = parse._toDict(data_in)
		result = dict_out[sec._parameter_]
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		del dict_out
		return
	
	@pytest.mark.parametrize("data_in,expected",pattern_parameterize_inline)
	def test_parameterize_inline(self,data_in,expected):
		dict_out = copy.deepcopy(sec.template)
		args = re.split(r"[ ]+",data_in)
		dict_out = parsers._parameterize_.inline(dict_out," ".join(args[2:]))
		result = dict_out[sec._parameter_]
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		del dict_out
		return




