###
### Loading default python modules
###
import os
import sys
import pathlib
import pytest
import copy
import re

from source.define_keyword import key
from source.define_keyword import sec 
from source import parse
import parsers._if_ 
import parsers._elseif_ 
import parsers._else_ 


class Test_parsers_if():
	pattern_if = []
	pattern_if.append(
		(
			[
				"!$fcg if ${x} == 4",
				"xxx = 4 ",
				"!$fcg elseif ${x}  <  0 ",
				"xxx = 0",
				"!$fcg elif ${x}>10",
				"xxx = 10",
				"!$fcg elif ${x} in [ 1,3, 5,7,9]",
				"xxx = 3",
				"!$fcg else ",
				"xxx = 5",
				"!$fcg end if",
			]
			,
			[
				[key._if_,"${x} == 4","xxx = 4 "],
				[key._elif_,"${x} < 0","xxx = 0"],
				[key._elif_,"${x}>10","xxx = 10"],
				[key._elif_,"${x} in [ 1,3, 5,7,9]","xxx = 3"],
				[key._else_,None,"xxx = 5"],
			]
		)
	)
	pattern_if.append(
		(
			[
				"!$fcg start if ${x} == 4",
				"xxx = 4 ",
				"!$fcg endif",
			]
			,
			[
				[key._if_,"${x} == 4","xxx = 4 "],
			]
		)
	)
	
	
	#####
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	#####
	#####
	@pytest.mark.parametrize("data_in,expected",pattern_if)
	def test_if(self,data_in,expected):
		dict_out = copy.deepcopy(sec.template)
		wrk = None 
		for line in data_in:
			args = re.split(r"[ ]+",line)
			if re.match(re.escape(key.prefix), args[0], flags=re.IGNORECASE) :
				del args[0]
				args[0] = key.toSystemKeyword(args[0])
				
				if args[0] == key._start_ :
					del args[0]
					del args[0]
					wrk = parsers._if_.wrk(" ".join(args))
				
				elif args[0] == key._if_ :
					del args[0]
					wrk = parsers._if_.wrk(" ".join(args))
				
				elif args[0] == key._elif_ :
					dict_out = wrk.finalize(dict_out)
					del wrk
					del args[0]
					wrk = parsers._elseif_.wrk(" ".join(args))
				
				elif args[0] == key._else_ :
					dict_out = wrk.finalize(dict_out)
					del wrk
					del args[0]
					wrk = parsers._else_.wrk(" ".join(args))
				
				elif args[0] in [ key._end_, key._endif_] :
					dict_out = wrk.finalize(dict_out)
					del wrk
					
				else :
					print(line)
					raise Exception()
			else :
				wrk.input(line)
				
			continue
		
		result = dict_out[sec._source_]
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		del dict_out
		return


	@pytest.mark.parametrize("data_in,expected",pattern_if)
	def test_parse(self,data_in,expected):
		dict_out = parse._toDict(data_in)
		result = dict_out[sec._source_]
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		del dict_out
		return



