###
### Loading default python modules
###
import os
import sys
import pathlib
import pytest
import copy
import re

from source import parse
from source.define_keyword import key
from source.define_keyword import sec 
import parsers._listup_ 

class Test_parsers_listup():
	pattern_listup = []
	pattern_listup.append(
		(
			[
				"!$fcg start listup i${i} for i = [1,2,3,4,5,6] ",
				"subroutine func(@{i})",
				"!$fcg end listup",
			]
			,
			{
				sec._task_ : key._listup_,
				sec._define_ : {
					"@{i}" : "i${i}",
					"#{i}" : ",",
				},
				sec._parameter_ : {
					"i" : [1,2,3,4,5,6],
				},
				sec._source_ : [
					"subroutine func(@{i})",
				]
			},
		)
	)
	pattern_listup.append(
		(
			[
				"!$fcg start listup i${i} at ipos do i in [1,2,3,4,5,6] ",
				"subroutine func(@{ipos})",
				"!$fcg end listup",
			]
			,
			{
				sec._task_ : key._listup_,
				sec._define_ : {
					"@{ipos}" : "i${i}",
					"#{ipos}" : ",",
				},
				sec._parameter_ : {
					"i" : [1,2,3,4,5,6],
				},
				sec._source_ : [
					"subroutine func(@{ipos})",
				]
			},
		)
	)
	pattern_listup.append(
		(
			[
				"!$fcg start listup i${i} at ipos do i in range(1,6+1) ",
				"subroutine func(@{ipos})",
				"!$fcg end listup",
			]
			,
			{
				sec._task_ : key._listup_,
				sec._define_ : {
					"@{ipos}" : "i${i}",
					"#{ipos}" : ",",
				},
				sec._parameter_ : {
					"i" : [1,2,3,4,5,6],
				},
				sec._source_ : [
					"subroutine func(@{ipos})",
				]
			},
		)
	)
	pattern_listup.append(
		(
			[
				"!$fcg start listup i${i} at ipos do i in range(1,${imax}+1) ",
				"subroutine func(@{ipos})",
				"!$fcg end listup",
			]
			,
			{
				sec._task_ : key._listup_,
				sec._define_ : {
					"@{ipos}" : "i${i}",
					"#{ipos}" : ",",
				},
				sec._parameter_ : {
					"i" : "range(1,${imax}+1)",
				},
				sec._source_ : [
					"subroutine func(@{ipos})",
				]
			},
		)
	)
	pattern_listup.append(
		(
			[
				"!$fcg start listup delimiter '+' format i${i} at ipos do i in [1,2,3,4,5,6] ",
				"total = @{ipos}",
				"!$fcg end listup",
			]
			,
			{
				sec._task_ : key._listup_,
				sec._define_ : {
					"@{ipos}" : "i${i}",
					"#{ipos}" : "+",
				},
				sec._parameter_ : {
					"i" : [1,2,3,4,5,6],
				},
				sec._source_ : [
					"total = @{ipos}",
				]
			},
		)
	)
	pattern_listup.append(
		(
			[
				"!$fcg start listup i${i} at ipos ",
				"subroutine func(@{ipos})",
				"!$fcg end listup",
			]
			,
			{
				sec._task_ : key._listup_,
				sec._define_ : {
					"@{ipos}" : "i${i}",
					"#{ipos}" : ",",
				},
				sec._parameter_ : {
				},
				sec._source_ : [
					"subroutine func(@{ipos})",
				]
			},
		)
	)

	#####
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	#####
	#####
	@pytest.mark.parametrize("data_in0,expected",pattern_listup)
	def test_listup(self,data_in0,expected):
		data_in = copy.deepcopy(data_in0)
		dict_out = copy.deepcopy(sec.template)
		args = re.split(r"[ ]+",data_in[0])
		if re.match(re.escape(key.prefix),args[0]) :
			del args[0]
			key_in = key.toSystemKeyword(args[0])
			if key_in == key._start_ :
				del args[0:2]
			elif key_in == key._listup_ :
				del args[0]
		wrk = parsers._listup_.wrk(" ".join(args))
		for data in data_in[1:-1] :
			wrk.input(data)
			continue
		dict_out = wrk.finalize(dict_out)
		result = dict_out
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		del wrk
		del dict_out
		return

	@pytest.mark.parametrize("data_in0,expected",pattern_listup)
	def test_parse(self,data_in0,expected):
		data_in = copy.deepcopy(data_in0)
		dict_out = parse._toDict(data_in)
		result = dict_out
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		del dict_out
		return

