###
### Loading default python modules
###
import os
import sys
import pathlib
import pytest
import copy
import re

from source import parse
from source.define_keyword import key
from source.define_keyword import sec 
import parsers._define_ 

class Test_parsers_define():
	pattern_define = []
	pattern_define.append(
		(
			[
				"!$fcg start define",
				"x : 4.0e-001",
				"i : [1,2,3,4]",
				'T : ["integer4","integer8"]',
				'str : ["integer(kind=4)","integer(kind=8)"]',
				'typedict : {"integer4":"i4","integer8":"i8"}',
				"!$fcg end define",
			]
			,
			{
				"x" : 4.0e-001,
				"i" : [1,2,3,4],
				"T" : ["integer4","integer8"],
				"str" : ["integer(kind=4)","integer(kind=8)"],
				"typedict" : { 
					"integer4" : "i4",
					"integer8" : "i8",
				},
			}
		)
	)

	pattern_define_inline = []
	pattern_define_inline.append(
		(
			"!$fcg define x = 4.0e-001",
			{
				"x" : 4.0e-001,
			}
		)
	)
	pattern_define_inline.append(
		(
			"!$fcg define i = [1,2,3,4]",
			{
				"i" : [1,2,3,4],
			}
		)
	)
	pattern_define_inline.append(
		(
			'!$fcg define T = ["integer4","integer8"]',
			{
				"T" : ["integer4","integer8"],
			}
		)
	)
	pattern_define_inline.append(
		(
			'!$fcg define str = ["integer(kind=4)","integer(kind=8)"]',
			{
				"str" : ["integer(kind=4)","integer(kind=8)"],
			}
		)
	)
	pattern_define_inline.append(
		(
			'!$fcg define typedict = {"integer4":"i4","integer8":"i8"}',
			{
				"typedict" : { 
					"integer4" : "i4",
					"integer8" : "i8",
				},
			}
		)
	)



	#####
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	#####
	#####
	@pytest.mark.parametrize("data_in,expected",pattern_define)
	def test_define(self,data_in,expected):
		dict_out = copy.deepcopy(sec.template)
		wrk = parsers._define_.wrk()
		for data in data_in[1:-1] :
			wrk.input(data)
			continue
		dict_out = wrk.finalize(dict_out)
		result = dict_out[sec._define_]
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		del wrk
		del dict_out
		return

	@pytest.mark.parametrize("data_in,expected",pattern_define)
	def test_parse(self,data_in,expected):
		dict_out = parse._toDict(data_in)
		result = dict_out[sec._define_]
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		del dict_out
		return

	@pytest.mark.parametrize("data_in,expected",pattern_define_inline)
	def test_define_inline(self,data_in,expected):
		dict_out = copy.deepcopy(sec.template)
		args = re.split(r"[ ]+",data_in)
		del args[0]
		del args[0]
		dict_out = parsers._define_.inline(dict_out," ".join(args))
		result = dict_out[sec._define_]
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		del dict_out
		return

	@pytest.mark.parametrize("data_in,expected",pattern_define_inline)
	def test_parser_inline(self,data_in,expected):
		dict_out = parse._toDict([data_in])
		result = dict_out[sec._define_]
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		del dict_out
		return




