###
### Loading default python modules
###
import os
import sys
import pathlib
import pytest
import copy
import re

from source import parse
from source.define_keyword import key
from source.define_keyword import sec 
import parsers._do_ 

class Test_parsers_do():
	pattern_do = []
	pattern_do.append(
		(
			[
				"!$fcg start do i in [1,2,3,4,5,6] ",
				"integer(4) :: i${i}",
				"!$fcg end do",
			]
			,
			{
				sec._task_ : key._generator_,
				sec._define_ : {},
				sec._parameter_ : {
					"i" : [1,2,3,4,5,6],
				},
				sec._source_ : [
					"integer(4) :: i${i}",
				]
			},
		)
	)
	pattern_do.append(
		(
			[
				"!$fcg do i in [1,2,3,4,5,6] ",
				"integer(4) :: i${i}",
				"!$fcg enddo",
			]
			,
			{
				sec._task_ : key._generator_,
				sec._define_ : {},
				sec._parameter_ : {
					"i" : [1,2,3,4,5,6],
				},
				sec._source_ : [
					"integer(4) :: i${i}",
				]
			},
		)
	)

	#####
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	#####
	#####
	@pytest.mark.parametrize("data_in,expected",pattern_do)
	def test_do(self,data_in,expected):
		dict_out = copy.deepcopy(sec.template)
		args = re.split(r"[ ]+",data_in[0])
		if re.match(re.escape(key.prefix),args[0]) :
			del args[0]
			key_in = key.toSystemKeyword(args[0])
			if key_in == key._start_ :
				del args[0:2]
			elif key_in == key._do_ :
				del args[0]
		wrk = parsers._do_.wrk(" ".join(args))
		for data in data_in[1:-1] :
			wrk.input(data)
			continue
		dict_out = wrk.finalize(dict_out)
		result = dict_out
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		del wrk
		del dict_out
		return

	@pytest.mark.parametrize("data_in,expected",pattern_do)
	def test_parse(self,data_in,expected):
		dict_out = parse._toDict(data_in)
		result = dict_out
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		del dict_out
		return

