###
### Loading default python modules
###
import os
import sys
import pathlib
import pytest
import tempfile

from source import transform
from source.define_keyword import key
from source.define_keyword import sec 

#####
#####
#####
pattern_file = []
pattern_file.append(
	"\n".join([
		"---",
		"define:",
		"  'i' : [1,2,3,4]",
		'  "T" : ["integer4","integer8"]',
		"parameterize:",
		"  'x' : [1.0,2.0,3.0,4.0]",
	])
)
pattern_file.append(
	"\n".join([
		"---",
		"define:",
		"  'j' : [4,5,6,7]",
		'  "U" : ["real4","real8"]',
		"parameterize:",
		"  'y' : [11.0,12.0]",
	])
)
	
#####
#####
class Test_transform():
	
	pattern_reform = []
	pattern_reform.append(
		(
			{
				"task" : "root",
				"define" : {},
				"parameterize" : {},
				"source" : [
					{
						"task" : "define",
						"define" : {
							"j" : [1,2,3],
						},
						"parameterize" : {},
						"source" : [],
					},
					"module test",
					{
						"task" : "generators",
						"define" : {},
						"parameterize" : {
							"i" : [1,2],
						},
						"source" : [
							"  integer, parameter :: i${i} = ${i} "
						],
					},
					"  contains",
					"end module",
				],
			}
		,
			{
				"task" : "root",
				"define" : {
					"j" : [1,2,3],
				},
				"parameterize" : {},
				"source" : [
					"module test",
					{
						"task" : "generators",
						"define" : {},
						"parameterize" : {
							"i" : [1,2],
						},
						"source" : [
							"  integer, parameter :: i${i} = ${i} "
						],
					},
					"  contains",
					"end module",
				],
			}
		)
	)
	
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	#####
	#####
	@pytest.mark.parametrize("data_in,expected",pattern_reform)
	def test_Levelup_DefAndParam(self,data_in,expected):
		result = transform.Levelup_DefAndParam(data_in)
		#print(data_in)
		#print(result)
		#print(expected)
		assert result == expected
		return
	
	#==================================================
	
	pattern_normalize = []
	pattern_normalize.append(
		(
			{
				"Parameterize" : { "i" : [1,2,3,4] },
				"Defines" : { "t" : ["integer4","integer8"] },
			}
			,
			{
				"parameterize" : { "i" : [1,2,3,4] },
				"define" : { "t" : ["integer4","integer8"] },
			}
		)
	)
	
	#####
	@pytest.mark.parametrize("data_in,expected",pattern_normalize)
	def test_transform_normalize(self,data_in,expected):
		result = transform._normalize(data_in)
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		return
	
	#==================================================
	
	pattern_reflectImport = []
	pattern_reflectImport.append(
		(
			{
				sec._task_      : key._import_,
				sec._define_    : {},
				sec._parameter_ : {},
				sec._source_    : [ 
					"file1.yml",
					"../file2.yml",
				],
			}
			,
			{
				sec._task_      : key._import_,
				sec._define_    : {
					"j" : [4,5,6,7],
					"i" : [1,2,3,4],
					"T" : ["integer4","integer8"],
					"U" : ["real4","real8"],
				},
				sec._parameter_ : {
					"y" : [11.0,12.0],
					"x": [1.0,2.0,3.0,4.0],
				},
				sec._source_    : [],
			}
		)
	)
	
	#####
	@pytest.mark.parametrize("data_in,expected",pattern_reflectImport)
	def test_transform_reflectImport(self,data_in,expected):
		result = {}
		with tempfile.TemporaryDirectory() as tmp_dir:
			pwd = tmp_dir + "/" + "tmp" 
			os.mkdir(pwd)
			for i, filename in enumerate(data_in[sec._source_]):
				f = open(pwd + "/" + filename,"w")
				f.write(pattern_file[i])
				f.close()
				continue
			
			result = transform.reflectImport(data_in,pwd)
		
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		
		return
	






