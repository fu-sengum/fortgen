###
### Loading default python modules
###
import os
import sys
import pathlib
import pytest
import yaml

from source import parse 

class Test_parse():
	pattern_toListInList = []
	pattern_toListInList.append(
		(
			[
				"    ",
				"!$fcg start generator",
				"!$fcg end generator",
			]
		,
			[
				"    ",
				[
					"!$fcg start generator",
					"!$fcg end generator",
				]
			]
		)
	)
	pattern_toListInList.append(
		(
			[
				"!$fcg start generators",
				"  !$fcg start generator",
				"  !$fcg end generator",
				"!$fcg end generators",
				"!$fcg start generators",
				"  !$fcg start generators",
				"    !$fcg start generator",
				"    !$fcg end generator",
				"    !$fcg start generator",
				"    !$fcg end generator",
				"  !$fcg end generators",
				"  !$fcg start generators",
				"    !$fcg start generator",
				"    !$fcg end generator",
				"  !$fcg end generators",
				"!$fcg end generators",
			],
			[
				[
					"!$fcg start generators",
					[
						"  !$fcg start generator",
						"  !$fcg end generator"
					],
					"!$fcg end generators",
				],
				[
					"!$fcg start generators",
					[
						"  !$fcg start generators",
						[
							"    !$fcg start generator",
							"    !$fcg end generator"
						],
						[
							"    !$fcg start generator",
							"    !$fcg end generator"
						],
						"  !$fcg end generators"
					],
					[
						"  !$fcg start generators",
						[
							"    !$fcg start generator",
							"    !$fcg end generator"
						],
						"  !$fcg end generators"
					],
					"!$fcg end generators"
				],
			],
		)
	)
	pattern_toListInList.append(
		(
			[
				"    ",
				"!$fcg start generator",
				"!$fcg define T = 1",
				"!$fcg parameterize i = [1,2,3,4]",
				"!$fcg end generator",
			]
		,
			[
				"    ",
				[
					"!$fcg start generator",
					["!$fcg define T = 1",],
					["!$fcg parameterize i = [1,2,3,4]",],
					"!$fcg end generator",
				]
			]
		)
	)
	pattern_toListInList.append(
		(
			[
				"    ",
				"!$fcg generator",
				"!$fcg define T = 1",
				"!$fcg parameterize",
				"i : [1,2,3,4]",
				"!$fcg end parameterize",
				"!$fcg end generator",
			]
		,
			[
				"    ",
				[
					"!$fcg generator",
					[
						"!$fcg define T = 1",
					],
					[
						"!$fcg parameterize", 
						"i : [1,2,3,4]",
						"!$fcg end parameterize",
					],
					"!$fcg end generator",
				]
			]
		)
	)
	
	
	pattern_parse = []
	pattern_parse.append(
		(
			"\n".join([
				"!$fcg start generator",
				"!$fcg parameterize i = [1,2,3,4]",
				"!$fcg parameterize T = ['integer(4)','integer(8)']",
				"subroutine prints(arg)",
				"  implicit none",
				"  ${T}, intent(in) :: arg",
				"  integer(4), parameter :: i = ${i}",
				"  !-----------------------",
				"  ",
				"  write(*,*) i,arg",
				"  ",
				"  return",
				"end subroutine",
				"!$fcg end generator",
			]),
			{
				"task" : "root",
				"define" : {},
				"parameterize" : {},
				"source" : [
					{
						"task" : "generator",
						"define" : {},
						"parameterize" : {
							"i" : [1,2,3,4],
							"T" : ["integer(4)","integer(8)"],
						},
						"source" : [
							"subroutine prints(arg)",
							"  implicit none",
							"  ${T}, intent(in) :: arg",
							"  integer(4), parameter :: i = ${i}",
							"  !-----------------------",
							"  ",
							"  write(*,*) i,arg",
							"  ",
							"  return",
							"end subroutine",
						],
					},
				],
			},
		)
	)
	pattern_parse.append(
		(
			"\n".join([
				"!$fcg start generators",
				"!$fcg parameterize i = [1,2,3,4]",
				"",
				"!$fcg start generator",
				"!$fcg parameterize T = ['integer(4)','integer(8)']",
				"subroutine prints(arg)",
				"  implicit none",
				"  ${T}, intent(in) :: arg",
				"  integer(4), parameter :: i = ${i}",
				"  !-----------------------",
				"  ",
				"  write(*,*) i,arg",
				"  ",
				"  return",
				"end subroutine",
				"!$fcg end generator",
				"",
				"!$fcg generator",
				"!$fcg parameterize",
				"T : ['real(4)','real(8)']",
				"!$fcg end parameterize",
				"subroutine prints(arg)",
				"  implicit none",
				"  ${T}, intent(in) :: arg",
				"  integer(4), parameter :: i = ${i}",
				"  !-----------------------",
				"  ",
				"  write(*,'(I2,F10.5)') i,arg",
				"  ",
				"  return",
				"end subroutine",
				"!$fcg end generator",
				"!$fcg end generators",
			]),
			{
				"task" : "root",
				"define" : {},
				"parameterize" : {},
				"source" : [
					{
						"task" : "generator",
						"define" : {},
						"parameterize" : {
							"i" : [1,2,3,4],
						},
						"source" : [
							"",
							{
								"task" : "generator",
								"define" : {},
								"parameterize" : {
									"T" : ["integer(4)","integer(8)"],
								},
								"source" : [
									"subroutine prints(arg)",
									"  implicit none",
									"  ${T}, intent(in) :: arg",
									"  integer(4), parameter :: i = ${i}",
									"  !-----------------------",
									"  ",
									"  write(*,*) i,arg",
									"  ",
									"  return",
									"end subroutine",
								],
							},
							"",
							{
								"task" : "generator",
								"define" : {},
								"parameterize" : {
									"T" : ['real(4)','real(8)'],
								},
								"source" : [
									"subroutine prints(arg)",
									"  implicit none",
									"  ${T}, intent(in) :: arg",
									"  integer(4), parameter :: i = ${i}",
									"  !-----------------------",
									"  ",
									"  write(*,'(I2,F10.5)') i,arg",
									"  ",
									"  return",
									"end subroutine",
								],
							},
						]
					},
				],
			},
		)
	)

	#####
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	#####
	#####
	@pytest.mark.parametrize("data_in,expected",pattern_toListInList)
	def test_toListInList(self,data_in,expected):
		result = parse._toListInList(data_in)
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		return

	@pytest.mark.parametrize("data_in,expected",pattern_parse)
	def test_execute(self,data_in,expected):
		result = parse.execute(data_in)
		print(data_in)
		print(yaml.dump(result, default_flow_style=False))
		print("")
		print(yaml.dump(expected, default_flow_style=False))
		assert result == expected
		return





