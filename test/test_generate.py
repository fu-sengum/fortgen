###
### Loading default python modules
###
import os
import sys
import copy
import pathlib
import pytest

from source import generate 
from source.define_keyword import key
from source.define_keyword import sec 

class Test_generate():
	pattern_updateDictVals = []
	pattern_updateDictVals.append(
		(
			{
				"T" : "${types}",
			}
			,
			{
				"types" : [1,2,3,4],
			}
			,
			{
				"T" : [1,2,3,4],
			}
		)
	)
	
	pattern_replace = []
	pattern_replace.append(
		(
			"integer(8), parameter :: arg = 2"
		,
		{
			"i" : 3,
			"T" : "integer4",
		}
		,
			"integer(8), parameter :: arg = 2"
		)
	)
	pattern_replace.append(
		(
			"${T}, parameter :: arg = ${i}"
		,
		{
			"i" : 3,
			"T" : "integer4",
		}
		,
			"integer4, parameter :: arg = 3"
		)
	)
	pattern_replace.append(
		(
			"subroutine equal_${abvs[${i}]}${abvs[${j}]}()"
		,
		{
			"i" : "real(8)",
			"j" : "integer(4)",
			"abvs" : {
					"integer(4)" : "i4",
					"integer(8)" : "i8",
					"real(4)" : "r4",
					"real(8)" : "r8",
				},
		}
		,
			"subroutine equal_r8i4()"
		,
		)
	)
	
	pattern_unpack = []
	pattern_unpack.append(
		(
			{
				sec._task_ : "root",
				sec._define_ : {},
				sec._parameter_ : {},
				sec._source_ : [
					{
						sec._task_ : key._generator_,
						sec._define_ : {},
						sec._parameter_ : {
							"T" : ['real(4)','real(8)'],
						},
						sec._source_ : [
							"subroutine prints(arg)",
							"  implicit none",
							"  ${T}, intent(in) :: arg",
							"  integer(4), parameter :: i = ${i}",
							"  !-----------------------",
							"  ",
							"  write(*,'(I2,F10.5)') i,arg",
							"  ",
							"  return",
							"end subroutine",
						],
					}
				],
			}
		,
			{
				"i" : 5,
			}
		,
			[
				"subroutine prints(arg)",
				"  implicit none",
				"  real(4), intent(in) :: arg",
				"  integer(4), parameter :: i = 5",
				"  !-----------------------",
				"  ",
				"  write(*,'(I2,F10.5)') i,arg",
				"  ",
				"  return",
				"end subroutine",
				"subroutine prints(arg)",
				"  implicit none",
				"  real(8), intent(in) :: arg",
				"  integer(4), parameter :: i = 5",
				"  !-----------------------",
				"  ",
				"  write(*,'(I2,F10.5)') i,arg",
				"  ",
				"  return",
				"end subroutine",
			]
		)
	)
	
	pattern_execute = []
	pattern_execute.append(
		(
			{
				sec._task_ : "root",
				sec._define_ : {},
				sec._parameter_ : {},
				sec._source_ : [
					{
						sec._task_ : key._generator_,
						sec._define_ : {},
						sec._parameter_ : {
							"i" : [1,2],
						},
						sec._source_ : [
							"",
							{
								sec._task_ : key._generator_,
								sec._define_ : {},
								sec._parameter_ : {
									"T" : ["integer(4)","integer(8)"],
								},
								sec._source_ : [
									"  ${T}, intent(in) :: arg",
									"  integer(4), parameter :: i = ${i}",
									"",
								],
							},
							"",
							{
								sec._task_ : key._generator_,
								sec._define_ : {},
								sec._parameter_ : {
									"T" : ['real(4)','real(8)'],
								},
								sec._source_ : [
									"  ${T}, intent(in) :: arg",
									"  integer(8), parameter :: i = ${i}",
									"",
								],
							},
						]
					},
				],
			}
		,
			[
				"",
				"  integer(4), intent(in) :: arg",
				"  integer(4), parameter :: i = 1",
				"",
				"  integer(8), intent(in) :: arg",
				"  integer(4), parameter :: i = 1",
				"",
				"",
				"  real(4), intent(in) :: arg",
				"  integer(8), parameter :: i = 1",
				"",
				"  real(8), intent(in) :: arg",
				"  integer(8), parameter :: i = 1",
				"",
				"",
				"  integer(4), intent(in) :: arg",
				"  integer(4), parameter :: i = 2",
				"",
				"  integer(8), intent(in) :: arg",
				"  integer(4), parameter :: i = 2",
				"",
				"",
				"  real(4), intent(in) :: arg",
				"  integer(8), parameter :: i = 2",
				"",
				"  real(8), intent(in) :: arg",
				"  integer(8), parameter :: i = 2",
				"",
			]
		)
	)
	pattern_execute.append(
		(
			{
				sec._task_ : "root",
				sec._define_ : {},
				sec._parameter_ : {},
				sec._source_ : [
					{
						sec._task_ : key._generator_,
						sec._define_ : {},
						sec._parameter_ : {
							"T" : ['integer','real','logical'],
						},
						sec._source_ : [
							{
								sec._task_ : key._if_,
								sec._define_ : {},
								sec._parameter_ : {},
								sec._source_ :[
									["elif","${T} == 'integer'","i = 0","u = 1"],
									["elif","${T} == 'real'","i = 0.0e-00",],
									["else",None,"i = null","u = .false."],
								],
							},
						],
					}
				],
			}
		,
			[
				"i = 0",
				"u = 1",
				"i = 0.0e-00",
				"i = null",
				"u = .false.",
			],
		)
	)
	pattern_execute.append(
		(
			{
				sec._task_ : key._listup_,
				sec._define_ : {
					"@{ipos}" : "i${i}",
					"#{ipos}" : ","
				},
				sec._parameter_ : {
					"i" : [1,2,3,4,5,6],
				},
				sec._source_ : [
					"subroutine func(@{ipos})",
				]
			}
		,
			[
				"subroutine func(i1,i2,i3,i4,i5,i6)",
			],
		)
	)
	pattern_execute.append(
		(
			{
				sec._task_ : key._listup_,
				sec._define_ : {
					"@{ipos}" : "i${i}",
					"#{ipos}" : " + "
				},
				sec._parameter_ : {
					"i" : [1,2,3,4,5,6],
				},
				sec._source_ : [
					"total = @{ipos}",
				]
			}
		,
			[
				"total = i1 + i2 + i3 + i4 + i5 + i6",
			],
		)
	)
	pattern_execute.append(
		(
			{
				sec._task_ : key._listup_,
				sec._define_ : {
					"nameroot" : "v",
					"@{ipos}" : "${nameroot}_${label}${i}",
					"#{ipos}" : ", "
				},
				sec._parameter_ : {
					"label" : ["i","r","c"],
					"i" : [1,2,3],
				},
				sec._source_ : [
					"subroutine func(@{ipos})",
				]
			}
		,
			[
				"subroutine func(v_i1, v_i2, v_i3, v_r1, v_r2, v_r3, v_c1, v_c2, v_c3)",
			],
		)
	)
	pattern_execute.append(
		(
			{
				sec._task_ : key._listup_,
				sec._define_ : {
					"nameroot" : "v",
					"@{ipos}" : "${nameroot}_${label}${i}",
					"#{ipos}" : ", ",
					"imax"    : 3,
				},
				sec._parameter_ : {
					"label" : ["i","r","c"],
					"i" : "range(1,${imax}+1)",
				},
				sec._source_ : [
					"subroutine func(@{ipos})",
				]
			}
		,
			[
				"subroutine func(v_i1, v_i2, v_i3, v_r1, v_r2, v_r3, v_c1, v_c2, v_c3)",
			],
		)
	)
	
	#####
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	#####
	#####
	@pytest.mark.parametrize("dict_target,dict_def,expected",pattern_updateDictVals)
	def test_updateDictVals(self,dict_target,dict_def,expected):
		result = generate._updateDictVals(dict_target,dict_def)
		print(dict_target)
		print(dict_def)
		print(result)
		print(expected)
		assert result == expected
		return

	@pytest.mark.parametrize("line,data_in,expected",pattern_replace)
	def test_replace(self,line,data_in,expected):
		result = generate._replace(line,data_in)
		print(line)
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		return

	@pytest.mark.parametrize("dict_in,param_in,expected",pattern_unpack)
	def test_unpack(self,dict_in,param_in,expected):
		result = generate._unpack(dict_in,param_in)
		print(dict_in)
		print(param_in)
		print(result)
		print(expected)
		assert result == expected
		return

	@pytest.mark.parametrize("list_in,expected",pattern_execute)
	def test_execute(self,list_in,expected):
		result = generate.execute(list_in)
		print(list_in)
		print(result)
		print(expected)
		assert result == expected
		return







