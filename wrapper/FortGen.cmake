###

cmake_minimum_required(VERSION 3.0)

find_package(Python3 COMPONENTS Interpreter)


#======================================
#======================================
#======================================

macro(LOAD_FCG_ENV)
	set(FCG_BINARY_DIRECTORY "${FCG_PACKAGE_ROOT}")
	set(FCG_EXECUTABLE "run.py")
	if (NOT EXISTS ${FCG_BINARY_DIRECTORY}/${FCG_EXECUTABLE})
		message(FATAL_ERROR "Not found FortGen executable!")
	endif()
	
	set(FCG_POSTFIX "__generated")
	
	set(FCG_OUTPUT_ROOT_DIR "${CMAKE_BINARY_DIR}/source_generated")
	if (NOT EXISTS ${FCG_OUTPUT_ROOT_DIR})
		file(MAKE_DIRECTORY ${FCG_OUTPUT_ROOT_DIR})
	endif()
	
	if (Python3_EXECUTABLE)
	else()
		message(FATAL_ERROR "Not defined of Python3_EXECUTABLE.")
	endif()
endmacro()

#=======================================

function(add_library_fcg NAME)
	LOAD_FCG_ENV()
	
	set(options "")
	set(sources "")
	set(datas   "")
	foreach(arg ${ARGN})
		if ( "${arg}" MATCHES "(STATIC|SHARED|MODULE|UNKNOWN|INTERFACE|OBJECT|EXCLUDE_FROM_ALL|IMPORTED|GLOBAL|ALIAS)" )
			list(APPEND options "${arg}")
		else()
			if ( "${arg}" MATCHES "/.*" )
				file(RELATIVE_PATH target ${CMAKE_CURRENT_SOURCE_DIR} "${arg}")
			else()
				set(target "${arg}")
			endif()
			
			get_filename_component(FILE_DIR "${target}" DIRECTORY)
			get_filename_component(FILE_EXT "${target}" EXT)
			get_filename_component(FILE_NAMEBODY "${target}" NAME_WE)
			set(OUTPUT_DIR "${FCG_OUTPUT_ROOT_DIR}/${FILE_DIR}")
			
			if(NOT EXISTS ${OUTPUT_DIR})
				file(MAKE_DIRECTORY ${OUTPUT_DIR})
			endif()
			
			if ( "${arg}" MATCHES ".*\\.(f90|F90|f|F|cuf|CUF)" )
				set(src "${OUTPUT_DIR}/${FILE_NAMEBODY}${FCG_POSTFIX}${FILE_EXT}")
				list(APPEND sources "${src}")
				
				if (TARGET "${src}")
				else()
					add_custom_command(
						OUTPUT "${src}"
						COMMAND ${Python3_EXECUTABLE} ${FCG_BINARY_DIRECTORY}/${FCG_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/${target} --postfix=${FCG_POSTFIX} --dest=${OUTPUT_DIR}
						DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${target}
					)
				endif()
			else()
				list(APPEND datas "${target}")
				add_custom_command(
					OUTPUT ${target}
					COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/${target} ${CMAKE_CURRENT_BINARY_DIR}/${target}
					DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${target}
				)
			endif()
		endif()
	endforeach()
	
	add_library(${NAME} ${options} ${sources})
	if ( DEFINED datas ) 
		add_custom_target(
			${NAME}-datacopy
			ALL DEPENDS ${datas}
		)
		add_dependencies(${NAME} ${NAME}-datacopy)
	endif()
endfunction()

#=======================================

function(add_executable_fcg NAME)
	LOAD_FCG_ENV()
	
	set(options "")
	set(sources "")
	set(datas   "")
	foreach(arg ${ARGN})
		if ( "${arg}" MATCHES "(WIN32|MACOSX_BUNDLE|EXCLUDE_FROM_ALL|IMPORTED|GLOBAL|ALIAS)" )
			list(APPEND options "${arg}")
		else()
			if ( "${arg}" MATCHES "/.*" )
				file(RELATIVE_PATH target ${CMAKE_CURRENT_SOURCE_DIR} "${arg}")
			else()
				set(target "${arg}")
			endif()
			#message(STATUS "${target} is processing...")
			
			get_filename_component(FILE_DIR "${target}" DIRECTORY)
			get_filename_component(FILE_EXT "${target}" EXT)
			get_filename_component(FILE_NAMEBODY "${target}" NAME_WE)
			set(OUTPUT_DIR "${FCG_OUTPUT_ROOT_DIR}/${FILE_DIR}")
			
			if(NOT EXISTS ${OUTPUT_DIR})
				file(MAKE_DIRECTORY ${OUTPUT_DIR})
				#message(STATUS "  ${OUTPUT_DIR} is created")
			endif()
			
			if ( "${arg}" MATCHES ".*\\.(f90|F90|f|F|cuf|CUF)" )
				set(src "${OUTPUT_DIR}/${FILE_NAMEBODY}${FCG_POSTFIX}${FILE_EXT}")
				list(APPEND sources "${src}")
				
				if (TARGET "${src}")
				else()
					add_custom_command(
						OUTPUT "${src}"
						COMMAND ${Python3_EXECUTABLE} ${FCG_BINARY_DIRECTORY}/${FCG_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/${target} --postfix=${FCG_POSTFIX} --dest=${OUTPUT_DIR}
						DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${target}
					)
				endif()
			else()
				list(APPEND datas "${target}")
				add_custom_command(
					OUTPUT ${target}
					COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/${target} ${CMAKE_CURRENT_BINARY_DIR}/${target}
					DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${target}
				)
			endif()
		endif()
	endforeach()
	
	add_executable(${NAME} ${options} ${sources})
	if ( DEFINED datas ) 
		add_custom_target(
			${NAME}-datacopy
			ALL DEPENDS ${datas}
		)
		add_dependencies(${NAME} ${NAME}-datacopy)
	endif()
endfunction()

