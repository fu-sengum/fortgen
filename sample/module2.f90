!$fcg start define
Tabs :
  "real(4)" : "r4" 
  "real(8)": "r8"
T0 :
  - "real(4)" 
  - "real(8)"
!$fcg end define

module constant2
  implicit none
  
  !$fcg start generator
  !$fcg parameterize T = ${T0}
  
  !--- Constant for ${T} --- 
  !$fcg start generator
  !$fcg parameterize i = range(0,11)
  !$fcg if ${T} == "real(4)"
  ${T}, parameter :: ${Tabs[${T}]}_${i} = ${i}.0e-00
  !$fcg elseif ${T} == "real(8)"
  ${T}, parameter :: ${Tabs[${T}]}_${i} = ${i}.0d-00
  !$fcg end if
  !$fcg end generator
  
  !$fcg end generator

end module
