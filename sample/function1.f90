!$fcg start define
shortname :
  "integer(4)" : "i4"
  "integer(8)" : "i8" 
  "real(4)" : "r4" 
  "real(8)": "r8"
!$fcg end define

!$fcg start generator
!$fcg start parameterize
type :
  - "integer(4)" 
  - "integer(8)" 
  - "real(4)" 
  - "real(8)"
!$fcg end parameterize
!$fcg
${type} function add_${shortname[${type}]}(a,b) result(val)
  implicit none
  ${type}, intent(in) :: a
  ${type}, intent(in) :: b
  !---------------------

  val = a + b
  return
end function 

!==============================================================================

!$fcg end generator

