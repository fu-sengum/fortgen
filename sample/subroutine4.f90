!$fcg define i_all = [1,2,3,4]
!$fcg start listup i${i} for i in ${i_all}
subroutine sumup(@{i},o1)
!$fcg end listup
  implicit none
  !$fcg start do i = ${i_all}
  integer(4), intent(in) :: i${i}
  !$fcg end do
  integer(4), intent(out) :: o1
  !---------------------
  
  !$fcg start do i = ${i_all}
  o1 = o1 + i${i}
  !$fcg end do

  return
end subroutine

!=============================================

!$fcg define imax = 4
!$fcg start listup i${i} for i in range(1,${imax}+1)
subroutine sumup2(@{i},o1)
!$fcg end listup
  implicit none
  !$fcg start do i = range(1,${imax}+1)
  integer(4), intent(in) :: i${i}
  !$fcg end do
  integer(4), intent(out) :: o1
  !---------------------
  
  !$fcg start do i = range(1,${imax}+1)
  o1 = o1 + i${i}
  !$fcg end do

  return
end subroutine

!=============================================

!$fcg start define 
i_all2 : [1,2,3]
body : 
  - ""
!$fcg end define
!$fcg start listup ${type}${i} at POS
!$fcg start parameterize
type:
  - "int"
  - "real"
i: ${i_all2} 
!$fcg end parameterize
subroutine mul(@{POS})
!$fcg end listup
  implicit none
  !$fcg start do type = ["int","real"]
  !$fcg start do i = ${i_all2}
  !$fcg if ${type} == "int"
  integer(4), intent(in)    :: ${type}${i}
  !$fcg elif ${type} == "real"
  integer(4), intent(inout) :: ${type}${i}
  !$fcg end if
  !$fcg end do
  !$fcg end do
  !---------------------
  
  !$fcg start do i = ${i_all2}
  real${i} = real${i} * int${i}
  !$fcg end do

  return
end subroutine

