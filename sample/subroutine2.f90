!$fcg start define
Tabs :
  "integer(4)" : "i4"
  "integer(8)" : "i8" 
  "real(4)" : "r4" 
  "real(8)": "r8"
!$fcg end define

!$fcg start generator
!$fcg start parameterize
T :
  - "integer(4)" 
  - "integer(8)" 
  - "real(4)" 
  - "real(8)"
!$fcg end parameterize
subroutine plus2_${Tabs[${T}]}(val)
  implicit none
  ${T}, intent(inout) :: val
  !---------------------
  
  !$fcg if "integer" in ${T} 
  val = val + 1
  !$fcg elif "real" in ${T}
  val = val + 1.0
  !$fcg else
  val = val
  !$fcg end if
  
  return
end subroutine

!$fcg end generator

