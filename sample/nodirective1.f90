module constant
  implicit none
!
! --- Numerical Constants (Real) ---
!

  integer*8, parameter :: ZERO_INT8 = 0
  integer*8, parameter :: ONE_INT8  = 1

  real*8, parameter :: ZERO   = 0.0d+00
  real*8, parameter :: ONE    = 1.0d+00
  real*8, parameter :: TWO    = 2.0d+00
  real*8, parameter :: THREE  = 3.0d+00
  real*8, parameter :: FOUR   = 4.0d+00
  real*8, parameter :: FIVE   = 5.0d+00
  real*8, parameter :: SIX    = 6.0d+00
  real*8, parameter :: SEVEN  = 7.0d+00
  real*8, parameter :: EIGHT  = 8.0d+00
  real*8, parameter :: NINE   = 9.0d+00
  real*8, parameter :: TEN    = 1.0d+01

  real*8, parameter :: SECOND  = 5.0d-01
  real*8, parameter :: THIRD   = 3.3333333333333333333333333333333d-01
  real*8, parameter :: FOURTH  = 2.5d-01
  real*8, parameter :: FIFTH   = 2.0d-01
  real*8, parameter :: SIXTH   = 1.6666666666666666666666666666667d-01
  real*8, parameter :: SEVENTH = 1.4285714285714285714285714285714d-01
  real*8, parameter :: EIGHTH  = 1.25d-01
  real*8, parameter :: NINTH   = 1.1111111111111111111111111111111d-01

end module Constant  


